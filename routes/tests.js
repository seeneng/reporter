/**
 * Upload test
 */
var fs = require('fs');
var klog = require('../lib/logging').kafkaLogger;
var cfg = require('../lib/config');
var mongoClient = require('mongodb').MongoClient;

	
var getCollectionRows = function(req, res, collName, query){
	res.set('Content-Type', 'application/json');
	mongoClient.connect(cfg.mongoUri, function(err, db){
		if (err == null){
			db.collection(collName).find(query).toArray(function(err, docs){
				if(err == null){
					var output = JSON.stringify(docs);
					res.send(output);
					res.end();
				}else{
					res.statusCode = 500;
					res.end();
				}
				db.close();
			});
		}else{
			res.statusCode = 500;
			res.end();
			klog.error('Unable to connect to MongoDB at ' + cfg.mongoUri);
			db.close();
		}
	});
}

exports.getClientLabels = function(req, res){
	getCollectionRows(req, res, 'releaseLabels', {});
}

exports.getDevices =  function(req, res){
	var rl = req.query.releaseLabel;
	if (!rl){
		res.statusCode = 400;
		res.end();
	}
	var query = { 'mcClientReleaseLabel' : rl };
	getCollectionRows(req, res, 'devices', query);
}

exports.upload = function(req, res){
	var partner = req.body.partner;
	var device = req.body.device;
	var file = req.files.file;
	
	mongoClient.connect(cfg.mongoUri, function(err, db){
		if(err == null){
			insertFile(db, partner, device, file, function(err, result){
				if(err != null){
					klog.error('Unable to insert data for device ' + device + ' of partner ' + partner, { status : 500, instanceID: 'admin' });
					res.status = 500;
					res.end();
					console.log('Cannot insert into db');
				}else{
					klog.info('Successfully inserted data for device ' + device + ' of partner ' + partner, { status : 200, instanceID: 'admin'});
					res.status = 200;
					res.end();
				}
				db.close();
			});
		}else{
			klog.error('Unable to connect to MongoDB for device data insertion');
			res.status(500);
			res.end();
			db.close();
		}
	});
};

var insertFile = function(db, partner, device, file, callback){
	var contents = fs.readFileSync(file.path, 'utf8');

	db.collection('uploads').
		insertOne({ 'name' : file.name, creationDate: new Date(), 'deviceId': device, 'type': 'file', 'ttl': 0, 'contents' : contents}, 
					function(err, result){
							callback(err, result);
					}
		);
}