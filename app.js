
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , realtime = require('./routes/realtime')
  , login = require('./routes/signin')
  , user = require('./routes/user')
  , tests = require('./routes/tests')
  , http = require('http')
  , path = require('path')
  , kafka = require('kafka-node')
  , socketIo = require('socket.io')
  , jade = require('jade')
  , messageProcessor = require('./lib/logProcessor')
  , multer = require('multer');
  
var app = express();

// all environments
app.set('port', process.env.PORT || 8888);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(multer({dest: '/tmp', rename: function (fieldname, filename) {
    return filename.replace(/\W+/g, '-').toLowerCase() + Date.now();
   }})
);

// development only
if ('development' === app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/realtime', realtime.update);
app.get('/users', user.list);
app.get('/signin', login.page);
app.get('/sdks', tests.getClientLabels);
app.get('/devices', tests.getDevices);
app.post('/upload', tests.upload);

var server = http.createServer(app);
var io = socketIo(server);

var sock;
io.on('connection', function(socket){
	console.log('socketIo connected');
	sock = socket;
	socket.on('disconnect', function(){
		console.log('socketIo disconnected');
		sock = null;
	});
});

app.set('socketio', io);

var jadeMessageFile = app.get('views') + '/message.jade';

//Listen to kafka topic, write to socket io
var kafkaClient = new kafka.Client();
var consumer = new kafka.Consumer(kafkaClient, [ {'topic' : 'magic3_log'} ]);
consumer.on('message', function(message){
	var msg = JSON.stringify(message);
	console.log('kafka consumer received ' + msg);
	console.log('sock = ' + sock);
	if(sock){
		console.log('broadcasting');
		msg = messageProcessor.sanitize(message);
		console.log('sanitized message = ' + JSON.stringify(msg));
		var html = jade.renderFile(jadeMessageFile, msg); 
		console.log('html = ' + html);
		//sock.broadcast.emit('log', html);
		io.emit('log', html);
	}
	//io.emit('log', msg);
});

server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
