var winston = require('winston');
var kafka = require('winston-kafka-transport');
var config = require('./config');

var logger = new (winston.Logger)({
	transports: [
	   //console logging results in I/O error when running in multi process config
	   new (winston.transports.Console)( { level: 'debug', handleExceptions: true, colorize: true, timestamp: true }),
	   new (winston.transports.DailyRotateFile)({ handleExceptions: true, filename: __dirname + '/../logs/reporter.log', level: 'debug'}),
	]
});

var kafkaLogger = new (winston.Logger)({
	transports: [
	   new (kafka)({ level : 'debug', topic : 'magic3_log', connectionString: config.kafkaUri, handleExceptions: false})
	]
});

logger.info('kafka uri = ' + config.kafkaUri);

exports.logger = logger;
exports.kafkaLogger = kafkaLogger;