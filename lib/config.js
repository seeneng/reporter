var fs = require('fs');

var REPORTER_CFG = '/home/ubuntu/magic3/conf/reporter.cfg';

//Defaults
var cfg = {};
cfg.mcfe = 'localhost:4000';
cfg.mongoUri = 'mongodb://localhost:27027/magic3_biz';
cfg.kafkaUri = 'localhost:2181'; 
	
var m3cfg; 
try{
	m3cfg = fs.readFileSync(REPORTER_CFG, 'utf8');
	m3 = JSON.parse(m3cfg);
	
	for (var k in m3){
		if (m3[k]){
			cfg[k] = m3[k];
			console.log("reporter.cfg - " + k + ' = ' + cfg[k]);
		}
	}
}catch(e){
	//Do nothing, use defaults
	console.log('Cannot read ' + REPORTER_CFG + ', using defaults, reason- ' + e.message);
}

module.exports = cfg;