/**
 * New node file
 */

var jade = require('jade');
var querystring = require('querystring');

var sanitize = function(original){
	var msg = {};
	console.log('sanitizing ' + original);
	console.log('value = ' + JSON.stringify(original.value));
	
	if (original && original.value){
		var value = JSON.parse(original.value);
		var kv = {
				'timestamp' : 'ts',
				'msg': 'msg',
				'level' : 'level'
		};
		
		for (var k in kv){
			console.log('k = ' + k + ' value = ' + value[k]);
			if (value[k]){
				msg[kv[k]] = value[k];
			}
		}
		
		var meta = value.meta;
		if (meta){
			var metaKv = {
					'mcClientReleaseLabel' : 'cl',
					'instanceID' : 'inst',
					'uriPath' : 'service',
					'statusCode' : 'rc',
					'stage' : 'stage',
					'status' : 'status'
			};
			
			for (var k in metaKv){
				console.log('k = ' + k + ' value = ' + meta[k]);
				if (meta[k]){
					msg[metaKv[k]] = meta[k];
				}
			}
			
			if (msg.ts){
				msg.ts = msg.ts.replace(/^\d{4}-/, '').replace('T', ' ').replace(/\.\d\d\dZ/, '');
			}
			
			if (msg.inst){
				msg.inst = msg.inst.split("_")[0];
			}
			
			if (msg.service){
				msg.service = msg.service.split("/")[1];
			}
			
			if (meta.fullUri){
				var qs = querystring.parse(meta.fullUri);
				if (qs.walletProviderID){
					msg.did = qs.walletProviderID;
				}
			}
		}
	}
	
	return msg;
};

module.exports = {
	sanitize: sanitize
};